<?php

namespace Desktime;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\RequestException;

/**
 * Class Desktime.
 */
class DesktimeClass
{
    protected \GuzzleHttp\Client $httpClient;

    private array $credentials;

    private string $url;

    private string $format;

    public function __construct()
    {
        $this->httpClient = new HttpClient();
        $this->credentials = [
            'api_key' => '',
        ];

        $url = 'https://desktime.com/api/v2';
        $this->setUrl($url);

        $this->setFormat('json');
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;
        return $this;
    }

    public function setCredentials(array $credentials): self
    {
        $this->credentials['api_key'] = $credentials['api_key'];
        return $this;
    }

    public function setFormat(string $format): self
    {
        $allowed_formats = ['json', 'jsonp', 'plist'];
        if (in_array(mb_strtolower($format), $allowed_formats)) {
            $this->format = mb_strtolower($format);
        }
        return $this;
    }

    public function buildUrl($service_path, $query = []): string
    {
        $url = $this->url;
        $url .= '/' . $this->format;
        $url .= '/' . $service_path;
        $url .= '?' . $this->buildQuery($query);
        return $url;
    }

    public function buildQuery(array $query): string
    {
        $query['apiKey'] = $this->credentials['api_key'];
        return http_build_query($query);
    }

    public function makeGetCall($url): object
    {
        $client = $this->httpClient;
        try {
            // @TODO: Allow override header values.
            $request = $client->get($url, [
                'verify' => FALSE,
                // 'timeout' => 1,
            ]);

            // If the HTTP status code is OK we will parse the body.
            if ($request->getStatusCode() == 200) {
                $result = (object)[
                    'pass' => TRUE,
                    'code' => $request->getStatusCode(),
                    // 'headers' => $request->getHeaders(), //.
                    'body' => (object)json_decode((string)$request->getBody()),
                ];
            } else {
                $result = (object)[
                    'pass' => FALSE,
                    'code' => $request->getStatusCode(),
                    'body' => [],
                ];
            }
        } catch (RequestException $e) {
            if ($e->getResponse() && $e->getResponse()->getBody() && $e->getResponse()->getBody()->getContents()) {
                $body = json_decode($e->getResponse()->getBody()->getContents());
                // $e->getResponse()->getReasonPhrase()
                // $e->getResponse()->getStatusCode() //.
                $body->http_response = $e->getResponse()->getReasonPhrase();
                $response_code = $e->getResponse()->getStatusCode();
            } else {
                // Can not fetch the response body contents.
                $body = 'FAILED_TO_FETCH_BODY_CONTENTS';
                $response_code = 0;
            }
            $result = (object)[
                'pass' => FALSE,
                'code' => $response_code,
                'body' => $body,
            ];
        }

        return $result;
    }
}

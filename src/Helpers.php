<?php

namespace Desktime;

class Helpers extends DesktimeClass
{
    private Employee $employee;

    private array $goals = [
        'desktime' => [
            'target' => (60 * 60 * 7),
            'minimum' => (60 * 60 * 6),
        ],
        'productive' => [
            'target' => (60 * 60 * 6),
            'minimum' => (60 * 60 * 5),
        ],
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function setEmployee(Employee $employee): self
    {
        $this->employee = $employee;
        return $this;
    }

    public function setGoals(array $goals): self
    {
        $this->goals = $goals;
        return $this;
    }

    public function getGoals(): array
    {
        return $this->goals;
    }

    public function getLastDays(int $limit = 5, array $week_days = [1, 2, 3, 4, 7]): array
    {
        // Source: https://stackoverflow.com/a/47750178/366884
        $date = date('Y/m/d');
        $ts = strtotime($date);
        // Limit + weekends.
        $offset = $limit + ($limit / 2.5) - 1;
        $ts = $ts - $offset * 86400;
        $last_work_days = [];
        for ($i = 0; $i < 9 + $offset; $i++, $ts += 86400) {
            if ($ts < time()) {
                if (in_array(date('N', $ts), $week_days)) { // day code is less then weekday 6 & 7
                    $last_work_days[] = date('Y-m-d', $ts);
                }
            }
        }
        return $last_work_days;
    }

    public function report(array $days, string $type = 'productive'): \stdClass
    {
        $result = new \stdClass();
        $result->pass = FALSE;
        $result->data = [];
        // @TODO: Validate $days as an array of dates.
        switch ($type) {
            case 'productive':
                $result->pass = TRUE;
                $result->data = self::reportProductive($days);
                break;
        }

        return $result;
    }

    private function reportProductive(array $days, $employee_id = FALSE): array
    {
        $report = [];
        $report['days'] = [];
        $report['short'] = [
            'desktime' => [
                'minimum' => [
                    'seconds' => 0,
                    'hours' => 0,
                ],
                'target' => [
                    'seconds' => 0,
                    'hours' => 0,
                ],
            ],
            'productive' => [
                'minimum' => [
                    'seconds' => 0,
                    'hours' => 0,
                ],
                'target' => [
                    'seconds' => 0,
                    'hours' => 0,
                ],
            ],
        ];
        foreach ($days as $date) {
            $employee = $this->employee->get(['date' => $date]);
            // print_r($employee);exit;
            $date_time = strtotime($date);

            $target_productive_goal = 0;
            if ($employee->body->productiveTime > $this->goals['productive']['target']) {
                $target_productive_goal = 1;
            }

            $minimum_productive_goal = 0;
            if ($employee->body->productiveTime > $this->goals['productive']['minimum']) {
                $minimum_productive_goal = 1;
            }

            $minimum_short_productive = 0;
            if ($employee->body->productiveTime < $this->goals['productive']['minimum']) {
                $minimum_short_productive = $this->goals['productive']['minimum'] - $employee->body->productiveTime;
                $report['short']['productive']['minimum']['seconds'] = $minimum_short_productive + $report['short']['productive']['minimum']['seconds'];
                $report['short']['productive']['minimum']['hours'] = gmdate('H:i:s', $report['short']['productive']['minimum']['seconds']);
            }

            $target_short_productive = 0;
            if ($employee->body->productiveTime < $this->goals['productive']['target']) {
                $target_short_productive = $this->goals['productive']['target'] - $employee->body->productiveTime;
                $report['short']['productive']['target']['seconds'] = $target_short_productive + $report['short']['productive']['target']['seconds'];
                $report['short']['productive']['target']['hours'] = gmdate('H:i:s', $report['short']['productive']['target']['seconds']);
            }

            $target_desktime_goal = 0;
            if ($employee->body->desktimeTime > $this->goals['desktime']['target']) {
                $target_desktime_goal = 1;
            }

            $minimum_desktime_goal = 0;
            if ($employee->body->desktimeTime > $this->goals['desktime']['minimum']) {
                $minimum_desktime_goal = 1;
            }

            $minimum_short_desktime = 0;
            if ($employee->body->desktimeTime < $this->goals['desktime']['minimum']) {
                $minimum_short_desktime = $this->goals['desktime']['minimum'] - $employee->body->desktimeTime;
                $report['short']['desktime']['minimum']['seconds'] = $minimum_short_desktime + $report['short']['desktime']['minimum']['seconds'];
                $report['short']['desktime']['minimum']['hours'] = gmdate('H:i:s', $report['short']['desktime']['minimum']['seconds']);
            }

            $target_short_desktime = 0;
            if ($employee->body->desktimeTime < $this->goals['desktime']['target']) {
                $target_short_desktime = $this->goals['desktime']['target'] - $employee->body->desktimeTime;
                $report['short']['desktime']['target']['seconds'] = $target_short_desktime + $report['short']['desktime']['target']['seconds'];
                $report['short']['desktime']['target']['hours'] = gmdate('H:i:s', $report['short']['desktime']['target']['seconds']);
            }

            $report['days'][] = [
                'day' => date('l', $date_time),
                'date' => date('Y/m/d', $date_time),
                'productive' => [
                    'time' => gmdate('H:i:s', $employee->body->productiveTime),
                    'time_seconds' => $employee->body->productiveTime,
                    'target' => $target_productive_goal,
                    'minimum' => $minimum_productive_goal,
                    'short' => [
                        'target' => [
                            'seconds' => $target_short_productive,
                            'hours' => gmdate('H:i:s', $target_short_productive),
                        ],
                        'minimum' => [
                            'seconds' => $minimum_short_productive,
                            'hours' => gmdate('H:i:s', $minimum_short_productive),
                        ],
                    ],
                ],
                'desktime' => [
                    'time' => gmdate('H:i:s', $employee->body->desktimeTime),
                    'time_seconds' => $employee->body->desktimeTime,
                    'target' => $target_desktime_goal,
                    'minimum' => $minimum_desktime_goal,
                    'short' => [
                        'target' => [
                            'seconds' => $target_short_desktime,
                            'hours' => gmdate('H:i:s', $target_short_desktime),
                        ],
                        'minimum' => [
                            'seconds' => $minimum_short_desktime,
                            'hours' => gmdate('H:i:s', $minimum_short_desktime),
                        ],
                    ],
                ],
            ];
        }

        return $report;
    }
}

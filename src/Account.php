<?php

namespace Desktime;

class Account extends DesktimeClass
{
    public function __construct()
    {
        parent::__construct();
    }

    public function company(): object
    {
        $url = 'company';
        $url = $this->buildUrl($url);
        return $this->makeGetCall($url, '');
    }
}
